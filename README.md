## License
* The license for the official SDK is the MIT license

## Compiling
This is a Catkin package. Make sure the package is on `ROS_PACKAGE_PATH` after cloning the package to your workspace. And the normal procedure for compiling a catkin package will work.

```
cd your_work_space
catkin_make
```
## Example Usage
**running the node**
```
rosrun imu_subs imu_listener
```
**Published Topics**

`imu/imu` (`sensor_msgs/Imu`)

The message contains the uncompensated (for the definition of UNCOMPENSATED, please refer to the [user manual](http://www.vectornav.com/docs/default-source/documentation/vn-100-documentation/UM001.pdf?sfvrsn=10)) angular velocity and linear acceleration. Note that the orientation is not provided on this topic.

`imu/magnetic_field` (`sensor_msgs/MagneticField`)

Uncompensated magnetic field.

`imu/pressure` (`sensor_msgs/FluidPressure`)

Pressure.

`imu/temperature` (`sensor_msgs/Temperature`)

Temperature in degree Celsius

